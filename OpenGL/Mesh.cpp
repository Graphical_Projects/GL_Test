#include "Mesh.h"
#include <iostream>
#include <fstream>
#include <string>

using std::cin;

Mesh::Mesh() : m_VAO(0), m_buffer(nullptr), m_bufNum(0)
{
}

Mesh::~Mesh()
{
	if (m_VAO)
	{
		glDeleteVertexArrays(1, &m_VAO);
	}
	if (m_bufNum != 0)
	{
		glDeleteBuffers(m_bufNum, m_buffer);
	}
	delete[] m_buffer;
}

void Mesh::loadMesh(const char* filename)
{
	std::vector<float> vertexData;
	std::vector<float> texData;
	std::vector<float> normalData;
	std::vector<unsigned int> indexData;

	std::streambuf *backup;

	std::ifstream infile(filename);

	backup = cin.rdbuf();
	cin.rdbuf(infile.rdbuf());

	while (!cin.eof())
	{
		char stringStart;
		cin >> stringStart;

		switch (stringStart)
		{
		case '#':
		{
			std::string temp;
			std::getline(cin, temp);
			break;
		}

		case 'v':
		{
			float vx, vy, vz;
			cin >> vx >> vy >> vz;

			vertexData.push_back(vx);
			vertexData.push_back(vy);
			vertexData.push_back(vz);

			break;
		}
		case 't':
		{
			float u, v;
			cin >> u >> v;

			texData.push_back(u);
			texData.push_back(v);

			break;
		}
		case 'n':
		{
			float nx, ny, nz;
			cin >> nx >> ny >> nz;

			normalData.push_back(nx);
			normalData.push_back(ny);
			normalData.push_back(nz);

			break;
		}
		case 'f':
		{
			unsigned int a, b, c;
			cin >> a >> b >> c;

			indexData.push_back(a);
			indexData.push_back(b);
			indexData.push_back(c);

			break;
		}
		case '\n':
		default:
			break;
		}
	}

	cin.rdbuf(backup);

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	m_bufNum = 4;
	m_buffer = new GLuint[m_bufNum];

	glGenBuffers(m_bufNum, m_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, vertexData.size() * 4, vertexData.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[1]);
	glBufferData(GL_ARRAY_BUFFER, normalData.size() * 4, normalData.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[2]);
	glBufferData(GL_ARRAY_BUFFER, texData.size() * 4, texData.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexData.size() * 4, indexData.data(), GL_STATIC_DRAW);
}