#include "auxMath.h"

namespace auxMath
{
	Matrix4x4::Matrix4x4() : m_field()
	{
		m_field = new float[4 * 4];
		for (int i = 0; i < 4 * 4; ++i)
			m_field[i] = 0.0f;
	}
	float& Matrix4x4::operator()(int i, int j)
	{
		return m_field[i * 4 + j];
	}
	const float Matrix4x4::operator()(int i, int j) const
	{
		return m_field[i * 4 + j];
	}
	const Matrix4x4 Matrix4x4::operator*(const Matrix4x4& rhs) const	// DEBUG THIS OR USE EXTERNAL LIBRARY
	{
		Matrix4x4 matrix;

		float temp = 0.0f;

		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				for (int k = 0; k < 4; ++k)
					matrix(i, j) += (*this)(k, j) * rhs(i, k);

		return matrix;
	}

	Vector3::Vector3(float p_x, float p_y, float p_z) : m_x(p_x), m_y(p_y), m_z(p_z) { }
	const Vector3 Vector3::operator*(const Vector3& rhs) const
	{
		return Vector3(m_x * rhs.x(), m_y * rhs.y(), m_z * rhs.z());
	}
	Vector3& Vector3::operator+=(const Vector3& rhs)
	{
		m_x += rhs.x();
		m_y += rhs.y();
		m_z += rhs.z();

		return *this;
	}
	Vector3 Vector3::operator-() const
	{
		return Vector3(-m_x, -m_y, -m_z);
	}
	void Vector3::normalize()
	{
		float length = sqrt(m_x*m_x + m_y*m_y + m_z*m_z);

		m_x /= length;
		m_y /= length;
		m_z /= length;
	}


	Matrix4x4 perspectiveProjMatrix(float p_near, float p_far, float p_right, float p_left, float p_top, float p_bottom)
	{
		Matrix4x4 projMatrix;

		float doubleNear = 2.0f * p_near;
		float rightMinusLeft = p_right - p_left;
		float topMinusBottom = p_top - p_bottom;
		float farMinusNear = p_far - p_near;

		projMatrix(0, 0) = doubleNear / rightMinusLeft;
		projMatrix(1, 1) = doubleNear / topMinusBottom;
		projMatrix(2, 0) = (p_right + p_left) / rightMinusLeft;
		projMatrix(2, 1) = (p_top + p_bottom) / topMinusBottom;
		projMatrix(2, 2) = -(p_far + p_near) / farMinusNear;
		projMatrix(2, 3) = -1.0f;
		projMatrix(3, 2) = -doubleNear*p_far / farMinusNear;

		return projMatrix;
	}
	Matrix4x4 perspectiveProjMatrix(float p_near, float p_far, float p_aspectRatio, float p_FOV)
	{
		float right = p_near * tan((p_FOV / 2.0f)*3.1415f / 180.0f);
		float left = -right;
		float top = right / p_aspectRatio;
		float bottom = -right / p_aspectRatio;

		return perspectiveProjMatrix(p_near, p_far, right, left, top, bottom);
	}

	Matrix4x4 identityMatrix()
	{
		Matrix4x4 matrix;

		for (int i = 0; i < 4; ++i)
			matrix(i, i) = 1.0f;

		return matrix;
	}
	Matrix4x4 translationMatrix(const Vector3& p_shift)
	{
		Matrix4x4 matrix;

		for (int i = 0; i < 4; ++i)
			matrix(i, i) = 1.0f;

		matrix(3, 0) = p_shift.x();
		matrix(3, 1) = p_shift.y();
		matrix(3, 2) = p_shift.z();

		return matrix;
	}
	Matrix4x4 rotationMatrix(const float p_angle, const Vector3& p_axis)
	{
		Matrix4x4 matrix;

		float radAngle = p_angle * piConst / 180.0f;

		float cosA = cos(radAngle);
		float oneMinusCosA = 1 - cosA;
		float sinA = sin(radAngle);

		matrix(0, 0) = cosA + p_axis.x()*p_axis.x()*oneMinusCosA;
		matrix(0, 1) = sinA*p_axis.z() + p_axis.x()*p_axis.y()*oneMinusCosA;
		matrix(0, 2) = -sinA*p_axis.y() + p_axis.x()*p_axis.z()*oneMinusCosA;

		matrix(1, 0) = -sinA*p_axis.z() + p_axis.x()*p_axis.y()*oneMinusCosA;
		matrix(1, 1) = cosA + p_axis.y()*p_axis.y()*oneMinusCosA;
		matrix(1, 2) = sinA*p_axis.x() + p_axis.y()*p_axis.z()*oneMinusCosA;

		matrix(2, 0) = sinA*p_axis.y() + p_axis.x()*p_axis.z()*oneMinusCosA;
		matrix(2, 1) = -sinA*p_axis.x() + p_axis.y()*p_axis.z()*oneMinusCosA;
		matrix(2, 2) = cosA + p_axis.z()*p_axis.z()*oneMinusCosA;

		matrix(3, 3) = 1.0f;

		return matrix;
	}
	Matrix4x4 scalingMatrix(const Vector3& p_scale)
	{
		Matrix4x4 matrix;

		matrix(0, 0) = p_scale.x();
		matrix(1, 1) = p_scale.y();
		matrix(2, 2) = p_scale.z();
		matrix(3, 3) = 1.0f;

		return matrix;
	}
} // namespace auxMath
