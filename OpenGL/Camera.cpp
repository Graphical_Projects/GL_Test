#include "Camera.h"

Camera::Camera() : Entity(), m_hAngle(0.0f), m_vAngle(0.0f), m_forward(false), m_backward(false), m_left(false), m_right(false), m_up(false), m_down(false)
{
}

Camera::~Camera()
{
}

void Camera::moveTo(Vector3 shift)
{
	Entity::moveTo(-shift);
}

void Camera::moveBy(Vector3 shift)
{
	Entity::moveBy(-shift);
}

void Camera::rotateTo(float angle, Vector3 axis)
{
	Entity::rotateTo(-angle, axis);
}

void Camera::rotateBy(float angle, Vector3 axis)
{
	Entity::rotateBy(-angle, axis);
}

Matrix4x4 Camera::viewMatrix() const
{
	//return auxMath::translationMatrix(-m_position) * auxMath::rotationMatrix(-m_rotAngle, m_axis);

	return m_modelMatrix;
}

void Camera::onKeyPress(int p_key, int p_scancode, int p_mods)
{
	if (p_key == GLFW_KEY_W)
		m_forward = true;
	if (p_key == GLFW_KEY_S)
		m_backward = true;
	if (p_key == GLFW_KEY_A)
		m_left = true;
	if (p_key == GLFW_KEY_D)
		m_right = true;
	if (p_key == GLFW_KEY_SPACE)
		m_up = true;
	if (p_key == GLFW_KEY_C)
		m_down = true;
}

void Camera::onKeyRelease(int p_key, int p_scancode, int p_mods)
{
	if (p_key == GLFW_KEY_W)
		m_forward = false;
	if (p_key == GLFW_KEY_S)
		m_backward = false;
	if (p_key == GLFW_KEY_A)
		m_left = false;
	if (p_key == GLFW_KEY_D)
		m_right = false;
	if (p_key == GLFW_KEY_SPACE)
		m_up = false;
	if (p_key == GLFW_KEY_C)
		m_down = false;
}

void Camera::onMouseMove(double xpos, double ypos)
{
	m_hAngle = -xpos;
	m_vAngle = -ypos;
}

void Camera::update(float dt)
{
	float xAxisMove = m_right - m_left;
	float zAxisMove = m_forward - m_backward;
	float yAxisMove = m_up - m_down;

	Vector3 m_direction(-sin(m_hAngle*auxMath::piConst / 180.0f), 0.0f, cos(m_hAngle*auxMath::piConst / 180.0f));

	m_position += Vector3(2.0f*dt * zAxisMove * m_direction.x(), 0.0f, -2.0f*dt * zAxisMove*m_direction.z());
	m_position += Vector3(2.0f*dt * xAxisMove * m_direction.z(), 0.0f, 2.0f*dt * xAxisMove*m_direction.x());
	m_position += Vector3(0.0, 2.0f*dt * yAxisMove, 0.0f);

	m_modelMatrix = auxMath::identityMatrix();

	moveBy(m_position);
	rotateBy(m_hAngle, Vector3(0.0f, 1.0f, 0.0f));
	rotateBy(m_vAngle, Vector3(1.0f, 0.0f, 0.0f));
}