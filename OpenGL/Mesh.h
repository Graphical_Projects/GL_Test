#pragma once

#include <gl\glew.h>
#include <vector>

class Mesh
{
public:
	Mesh();
	~Mesh();

	void loadMesh(const char* filename);
private:
	GLuint m_VAO;
	GLuint *m_buffer;
	int m_bufNum;
};

