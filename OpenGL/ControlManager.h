#pragma once
#include <vector>

#include "Entity.h"

class ControlManager
{
public:
	ControlManager();
	~ControlManager();

	void addListener(Entity *p_listener);

	void keyPressNotify(int p_key, int p_scancode, int p_mods);
	void keyReleaseNotify(int p_key, int p_scancode, int p_mods);

	void mouseMoveNotify(double xpos, double ypos);

private:
	std::vector<Entity*> m_listeners;
};

