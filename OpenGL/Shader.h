#pragma once

#include <memory>
#include <gl\glew.h>

class Shader
{
public:
	Shader(GLenum type);
	~Shader();

	bool loadShader(const char *source);
	GLuint getShader() const;

private:
	std::shared_ptr<GLuint> shaderObject;
};

