#include "Texture.h"
#include <cstring>


Texture::Texture(const char *source)
{
	ilGenImages(1, &m_image);
	ilBindImage(m_image);
	ilLoadImage(source);

	m_width = ilGetInteger(IL_IMAGE_WIDTH);
	m_height = ilGetInteger(IL_IMAGE_HEIGHT);
	m_bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);

	ILubyte *imageData = ilGetData();
	char *tempBlock = new char[m_width*m_bpp];

	// Turning image back to normal from upside down
	for (int i = 0; i < (m_height / 2); ++i)
	{
		memcpy(tempBlock, imageData + i*m_width*m_bpp, m_width*m_bpp);
		memcpy(imageData + i*m_width*m_bpp, imageData + (m_height-1-i)*m_width*m_bpp, m_width*m_bpp);
		memcpy(imageData + (m_height - 1 - i)*m_width*m_bpp, tempBlock, m_width*m_bpp);
	}

	delete[] tempBlock;

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, 0);
}


Texture::~Texture()
{
	ilDeleteImage(m_image);
	glDeleteTextures(1, &m_texture);
}


GLuint Texture::texObj() const
{
	return m_texture;
}