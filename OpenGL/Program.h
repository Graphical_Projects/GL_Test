#pragma once

#include <gl\glew.h>
#include <vector>
#include <memory>

#include "Shader.h"

class Program
{
public:
	Program();
	~Program();

	bool linkProgram(std::vector<Shader> &shaders);
	void useProgram();
	GLuint getProgram() const;
private:
	std::shared_ptr<GLuint> programObject;
};

